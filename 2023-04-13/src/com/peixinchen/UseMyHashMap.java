package com.peixinchen;

import java.util.Random;

public class UseMyHashMap {
    private static final String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    private static final Random rand = new Random();

    private static String randomString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 100; i++) {
            int idx = rand.nextInt(chars.length());
            char randChar = chars.charAt(idx);
            sb.append(randChar);
        }

        return sb.toString();
    }

    public static void main(String[] args) {
        MyHashMap map = new MyHashMap();
        for (int i = 0; i < 1000; i++) {
            String key = randomString();
            Long value = rand.nextLong();
            map.put(key, value);
        }

        System.out.println(map.maxSize());
    }
}
