package com.peixinchen;

// 1. 使用 Map 模型，Set 模型只是结点中不保存 value
// 2. 选择 Key 类型为引用类型 String
// 3. 选择 String 类型为 Long
// 3. 使用单链表维护桶中的链表
public class MyHashMap {
    public int maxSize() {
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < array.length; i++) {
            int count = 0;
            Node cur = array[i];
            while (cur != null) {
                count++;
                cur = cur.next;
            }
            if (count > max) {
                max = count;
            }
        }

        return max;
    }

    private static class Node {
        String key;
        Long value;
        Node next;

        Node(String key, Long value) {
            this.key = key;
            this.value = value;
        }
    }

    // 哈希表的桶是由一个数组组成，请问数组的元素类型是 Node 类型的引用
    // 保存就是链表的头节点
    private Node[] array;
    private int size;   // key-value 的个数

    public MyHashMap() {
        this.array = new Node[8];
        this.size = 0;
    }

    // assert key != null
    public Long put(String key, Long value) {
        // 1. 需要先查找，确定 key 是否存在
        // 2. String -> int (>= 0)   Java 中有约定
        // Object 类有一个 hashCode() 方法
        // 返回这个对象的一个 hash 值
        // 默认实现：JVM 内部返回一个不会重复的数值，每个对象都是唯一的
        int h = key.hashCode(); // h 只是一个 >= 0 的整数
        // 3. 把 h 变成 [0, array.length) 的取值，才能对应到一个合法的下标
        // int index = h % array.length;   // 方法1：除留余数法
        // 方法2：JDK 内部的 HashMap 使用的方法（前提：array.length 总是 2 的幂次方）
        // array.length in (2, 4, 8, 16, 32, 64, ...)
        // 3.1 把 h 的高 16 位取出来
        int h16 = h >> 16;
        // 3.2 把 h 的低 16 位取出来
        int l16 = 0xFF & h;
        h = h16 ^ l16;
        int index = h & (array.length - 1);

        // 4. 根据下标找到对应链表的头结点
        Node head = array[index];
        Node cur = head;
        while (cur != null) {
            // 比较 key 和 cur.key 是否相等（比较同一性还是比较相等性？）
            // 对象的相等性比较使用 equals 方法比较
            if (key.equals(cur.key)) {
                // 存在 key 了
                // 更新 value 即可
                Long oldValue = cur.value;
                cur.value = value;
                return oldValue;
            }

            cur = cur.next;
        }

        // 没有找到 key，进行插入
        Node node = new Node(key, value);
        // 往 head 对应的链表插入，头插/尾插理论都可以
        // JDK7 使用的是头插，简单。
        node.next = head;
        // head = node;    // head 只是个临时变量，不能这么改
        array[index] = node;

        size++;

        // 把负载因子的控制考虑起来，即 size / array.length >= 某个阈值时，扩容
        if (1.0 * size / array.length >= 0.75) {
            grow();
        }

        return null;
    }

    private void grow() {
        // 扩容时，仍然要保存 array.length 是 2 的幂次方
        int newLength = array.length * 2;
        Node[] newArray = new Node[newLength];

        // 和顺序表扩容不同，不能简单地把数组的元素搬过来
        // 元素 e 的下标是和 array.length 有关系的
        // array.length 变了，下标可能会变化，所以需要每个 key 都需要重新计算下标，重新插入
//        for (int i = 0; i < array.length; i++) {
//            newArray[i] = array[i];
//        }

        // 如何把哈希表中的所有元素都遍历到
        for (int i = 0; i < array.length; i++) {
            Node head = array[i];
            Node cur = head;
            while (cur != null) {
                Node next = cur.next;
                int h = cur.key.hashCode();
                int h16 = h >> 16;
                int l16 = 0xFF & h;
                h = h16 ^ l16;
                int index = h & (newLength - 1);

                // 使用头插法
                cur.next = newArray[index];
                newArray[index] = cur;

                cur = next;
            }
        }

        this.array = newArray;
    }

    public Long get(String key) {
        int h = key.hashCode();
        int h16 = h >> 16;
        int l16 = 0xFF & h;
        h = h16 ^ l16;
        int index = h & (array.length - 1);

        for (Node cur = array[index]; cur != null; cur = cur.next) {
            if (key.equals(cur.key)) {
                return cur.value;
            }
        }

        return null;
    }

    public Long remove(String key) {
        int h = key.hashCode();
        int h16 = h >> 16;
        int l16 = 0xFF & h;
        h = h16 ^ l16;
        int index = h & (array.length - 1);

        Node prev = null;
        for (Node cur = array[index]; cur != null; prev = cur, cur = cur.next) {
            if (key.equals(cur.key)) {
                Long value = cur.value;
                if (prev == null) {
                    // 说明 cur 是头节点
                    array[index] = cur.next;
                } else {
                    prev.next = cur.next;
                }
                size--;
                return value;
            }
        }

        return null;
    }
}
