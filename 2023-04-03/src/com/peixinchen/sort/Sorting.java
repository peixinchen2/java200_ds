package com.peixinchen.sort;

import java.util.Arrays;

public class Sorting {
    // 对 array 的前 size 个元素进行排序
    public static void bubbleSort(long[] array, int size) {
        // 外部的循环，写我们需要多次冒泡过程
        for (int i = 0; i < size - 1; i++) {
            // 无序区间: [0, size - i)
            // 有序区间: [size - i, size)

            // 内部的循环，通过两两比较并保证最大的元素在两个的后边
            // 冒泡过程
            // 只需要经历无序区间
            // j 代表黑色箭头
            boolean sorting = true;

            for (int j = 0; j < size - i - 1; j++) {
                if (array[j] > array[j + 1]) {
                    swap(array, j, j + 1);
                    sorting = false;
                }
            }

            if (sorting) {
                break;
            }
        }
    }

    private static void swap(long[] array, int i, int j) {
        long t = array[i];
        array[i] = array[j];
        array[j] = t;
    }

    // 对 array 的 [fromIndex, toIndex)
    // fromIndex 和 toIndex 一定合法
    public static void bubbleSortRange(long[] array, int fromIndex, int toIndex) {
        for (int i = fromIndex; i < toIndex - 1; i++) {
            // toIndex - i 是无序区间的长度，toIndex + fromIndex - i 是 无序区间开始下标 + 长度
            // 无序区间: [fromIndex, toIndex + fromIndex - i)
            // 有序区间: [toIndex + fromIndex - i, toIndex)
            for (int j = fromIndex; j < toIndex + fromIndex - i - 1; j++) {
                if (array[j] > array[j + 1]) {
                    swap(array, j, j + 1);
                }
            }
        }
    }

    public static void selectSort2(long[] array, int size) {
        // [有序（小）][无序][有序（大）]
        int leftIdx = 0;
        int rightIdx = size;
        // 有序（小）: [0, leftIdx)
        // 无序:       [leftIdx, rightIdx)
        // 有序（大）：[rightIdx, size)
        // 无序区间的个数: rightIdx - leftIdx

        // 循环继续的条件含义：无序区间的元素个数 >= 1
        while (rightIdx - leftIdx >= 1) {
            int minIdx = leftIdx;
            int maxIdx = leftIdx;
            // 遍历整个无序区间
            for (int i = leftIdx + 1; i < rightIdx; i++) {
                if (array[i] < array[minIdx]) {
                    minIdx = i;
                }
                if (array[i] > array[maxIdx]) {
                    maxIdx = i;
                }
            }

            // 交换最小的元素[minIdx]到无序区间的最开始[leftIdx]
            // 交换最大的元素[maxIdx]到无序区间的最后[rightIdx - 1]
            swap(array, minIdx, leftIdx);
            if (maxIdx == leftIdx) {
                // 找到的最大的元素放在无序区间的最开始
                // 随着 swap(array, minIdx, leftIdx) 交换之后，最大元素的下标变化
                maxIdx = minIdx;
            }
            swap(array, maxIdx, rightIdx - 1);

            leftIdx++;
            rightIdx--;
        }
    }

    public static void selectSort(long[] array, int size) {
        // 一共需要 size - 1 次选择过程
        for (int i = 0; i < size - 1; i++) {
            // 无序区间: [0, size - i)
            // 有序区间: [size - i, size)
            // 无序区间的最后一个位置的下标 [size - i - 1]

            // 进行选择过程，遍历整个无序区间，找到当前无序区间最大的元素下标
            int maxIdx = 0;
            for (int j = 1; j < size - i; j++) {
                if (array[j] > array[maxIdx]) {
                    maxIdx = j;
                }
            }
            // 无序区间的最大元素的下标 maxIdx
            swap(array, maxIdx, size - i - 1);
        }
    }

    public static void insertSort(long[] array, int size) {
        for (int i = 1; i < size; i++) {    // i 的含义，要插入的元素
            // 插入排序关注有序区间
            // 有序区间: [0, i)
            // 无序区间: [i, size)
            long key = array[i];
            // 倒着遍历有序区间，找到合适的位置
            // 合适的位置?
            // key < array[j]
            // key == array[j]      插入 [j + 1]
            // key > array[j]       插入 [j + 1]
            int j;
            for (j = i - 1; j >= 0 && key < array[j]; j--) {
                array[j + 1] = array[j];
            }
            // 循环结束，说明找到合适的位置
            array[j + 1] = key;
        }
    }

    public static void main(String[] args) {
        long[] array = { 9, 3, 2, 6, 8, 5, 4, 1, 7 };
        insertSort(array, array.length);
        System.out.println(Arrays.toString(array));
    }
}
