package com.peixinchen.use_pq;

import java.util.Comparator;
import java.util.PriorityQueue;

public class Main1 {
    static class ReverseIntegerComparator implements Comparator<Integer> {
        @Override
        public int compare(Integer o1, Integer o2) {
            int i1 = o1;
            int i2 = o2;
            if (i1 < i2) {  // int < int    7   9
                // o1 大于 o2
                return 1;
            } else if (i1 == i2) {
                return 0;
            } else {
                return -1;
            }
        }
    }

    public static void main(String[] args) {
        ReverseIntegerComparator c = new ReverseIntegerComparator();
        int cmp = c.compare(7, 9);
        if (cmp < 0) {
            System.out.println("7 小于 9");
        } else if (cmp == 0) {
            System.out.println("7 等于 9");
        } else {
            System.out.println("7 大于 9");
        }

        // 优先级队列的队首元素是 “最小” 的元素
        // 但我们可以通过比较器重新定义 “大小关系”
        // 强行定义   9 小于 7 小于 5 小于 3 小于 2
        PriorityQueue<Integer> pq = new PriorityQueue<>(c);

        pq.offer(7);
        pq.offer(9);
        pq.offer(3);
        pq.offer(5);
        pq.offer(2);

        System.out.println(pq.peek());  // 希望看到 9
    }

    public static void main1(String[] args) {
        // Integer 具备比较能力（实现了 Comparable 接口）
        PriorityQueue<Integer> pq = new PriorityQueue<>();  // 无参，没有比较器

        pq.offer(7);
        pq.offer(9);
        pq.offer(3);
        pq.offer(5);
        pq.offer(2);

//        System.out.println(pq.peek());
        while (!pq.isEmpty()) {
            System.out.println(pq.poll());
        }
    }
}
