package com.peixinchen.use_pq;

import java.util.PriorityQueue;

// 2 3 4 ... J Q K A
// ♥ < ♦ < ♠ < ♣
// Card 具备了比较能力
public class Card implements Comparable<Card> {
    public String suit; // 花色   ♥、♦、♠、♣
    public int rank;    // 面值   1-13

    public Card(String suit, int rank) {
        this.suit = suit;
        this.rank = rank;
    }

    @Override
    public int compareTo(Card o) {
        int thisRank = transRank();
        int thisSuit = transSuit();
        int oRank = o.transRank();
        int oSuit = o.transSuit();

        if (thisRank != oRank) {    // 如果 面值不同，则按照面值比较
            return thisRank - oRank;
        }

        return thisSuit - oSuit;    // 如果面值相同，按照花色比较
    }

    private int transRank() {
        if (this.rank == 1) {
            return 14;
        }

        return this.rank;
    }

    private int transSuit() {
        switch (this.suit) {
            case "♥": return 0;
            case "♦": return 1;
            case "♠": return 2;
            case "♣": return 3;
        }

        return -1;
    }

    @Override
    public String toString() {
        return "Card{" +
                "suit='" + suit + '\'' +
                ", rank=" + rank +
                '}';
    }

    public static void main(String[] args) {
        PriorityQueue<Card> pq = new PriorityQueue<>();

        pq.offer(new Card("♥", 3));
        pq.offer(new Card("♠", 5));
        pq.offer(new Card("♣", 2));

        while (!pq.isEmpty()) {
            System.out.println(pq.poll());
        }
    }
}
