package leetcode.p118;

import java.util.*;

class Solution {
    public List<List<Integer>> generate(int numRows) {
        List<List<Integer>> ans = new ArrayList<>();
        List<Integer> row = new ArrayList<>();
        row.add(1);
        ans.add(row);

        if (numRows == 1) {
            return ans;
        }

        row = new ArrayList<>();
        row.add(1);
        row.add(1);
        ans.add(row);

        if (numRows == 2) {
            return ans;
        }

        for (int i = 2; i < numRows; i++) {
            row = new ArrayList<>();
            row.add(1);
            // 第 3 行，中间只有一个元素
            List<Integer> lastRow = ans.get(i - 1);
            for (int j = 1; j < i; j++) {
                int a = lastRow.get(j - 1);
                int b = lastRow.get(j);
                int c = a + b;
                row.add(c);
            }

            row.add(1);
            ans.add(row);
        }

        return ans;
    }
}
