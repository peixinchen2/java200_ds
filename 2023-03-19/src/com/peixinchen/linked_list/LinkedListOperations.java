package com.peixinchen.linked_list;

public class LinkedListOperations {
    public static void 定义一条empty的链表() {
        Node head = null;
    }

    public static Node 定义一条4个元素的链表() {
        // [1, 2, 3, 4]

        Node n1 = new Node(1);
        Node n2 = new Node(2);
        Node n3 = new Node(3);
        Node n4 = new Node(4);

        n1.next = n2;
        n2.next = n3;
        n3.next = n4;
        n4.next = null;

        Node head = n1;

        return head;
    }

    public static void 遍历并且打印每个元素(Node head) {
        System.out.print("遍历打印链表：");
        for (Node cur = head; cur != null; cur = cur.next) {
            System.out.print(cur.val + " -> ");
        }

        System.out.println("null");
    }

    public static int 统计链表的长度(Node head) {
        int count = 0;
        for (Node cur = head; cur != null; cur = cur.next) {
            count++;
        }

        return count;
    }

    public static Node 头插(Node head, int val) {
        Node node = new Node(val);

        node.next = head;

        return node;
    }

    public static Node 头删(Node head) {
        return head.next;
    }

    public static void main2(String[] args) {
        Node head = null;
        head = 头插(head, 1);
        head = 头插(head, 2);
        head = 头插(head, 3);
        head = 头插(head, 4);

        // 4 -> 3 -> 2 -> 1 -> null
        遍历并且打印每个元素(head);

        head = 头删(head);
        // 3 -> 2 -> 1 -> null
        遍历并且打印每个元素(head);

        head = 头删(head);
        // 2 -> 1 -> null
        遍历并且打印每个元素(head);

        head = 头删(head);
        // 1 -> null
        遍历并且打印每个元素(head);

        head = 头删(head);
        // null
        遍历并且打印每个元素(head);
    }

    public static Node 尾插(Node head, int val) {
        Node node = new Node(val);
        if (head == null) {
            return node;
        }

        Node last = head;
        while (last.next != null) {
            last = last.next;
        }
        last.next = node;

        return head;
    }

    public static Node 尾删(Node head) {
        if (head.next == null) {
            return null;
        }

        Node ll = head;
        while (ll.next.next != null) {
            ll = ll.next;
        }

        ll.next = null;

        return head;
    }

    public static void main(String[] args) {
        Node head = null;
        head = 尾插(head, 1);
        head = 尾插(head, 2);
        head = 尾插(head, 3);
        head = 尾插(head, 4);

        // 1 -> 2 -> 3 -> 4 -> null
        遍历并且打印每个元素(head);

        head = 尾删(head);
        // 1 -> 2 -> 3 -> null
        遍历并且打印每个元素(head);

        head = 尾删(head);
        // 1 -> 2 -> null
        遍历并且打印每个元素(head);

        head = 尾删(head);
        // 1 -> null
        遍历并且打印每个元素(head);

        head = 尾删(head);
        // null
        遍历并且打印每个元素(head);
    }

    public static void main1(String[] args) {
//        Node head = 定义一条4个元素的链表();
        Node head = null;

        遍历并且打印每个元素(head);
        // 备注：要完整的打印链表，必须进行链表的遍历
        System.out.println(head);   // 这个无法打印链表，仅仅是打印了头结点对象

        System.out.println(统计链表的长度(head));
    }
}
