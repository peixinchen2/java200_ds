package com.peixinchen.array_list;

import java.util.Arrays;
import java.util.Collection;

public class MyArrayList {
    private Long[] array;
    private int size;

    public MyArrayList() {
        array = new Long[4];   // 引用类型，初始值是 null
        size = 0;
    }

    public MyArrayList(int capacity) {
        array = new Long[capacity];
        size = 0;
    }

    public MyArrayList(Collection c) {
        // 等价于先创建空的顺序表
        // this.addAll(c);
    }

    public static void main(String[] args) {
        // 创建了顺序表：容量 = 100， 元素个数 = 0
        MyArrayList list = new MyArrayList(100);

        // 获取下标为 5 的元素
        // 下标的合法性之和元素个数有关
        // [0, 0)
        Long e = list.get(0);
        System.out.println(e);
    }


    // 确保容量至少够放入一个元素
    // O(n)
    private void ensureCapacity() {
        if (size < array.length) {
            return;
        }

        array = Arrays.copyOf(array, array.length * 2);
//
//        // 先确定新数组容量大小（使用原大小的两倍）
//        int newCapacity = array.length * 2;
//        // 创建一个新的数组
//        Long[] newArray = new Long[newCapacity];
//        // 把所有元素从原数组搬到新数组
//        for (int i = 0; i < size; i++) {
//            newArray[i] = array[i];
//        }
//        // 让 array 引用指向新数组
//        array = newArray;
    }

    // array = [a, b], size = 2
    // add(z)
    // array = [a, b, z], size = 3
    // 时间复杂度：
    // 1）数据规模：n = size
    // f(n) = C
    // 时间复杂度: O(1)   备注：考虑到扩容的话，会退化成 O(n)，但一般不考虑扩容
    public boolean add(Long e) {
        ensureCapacity();

        array[size] = e;
        size = size + 1;

        return true;
    }

    // 时间复杂度：头插是最坏情况
    // 数据规模：元素个数 size
    // 时间复杂度: O(n)
    public void add(int index, Long e) {
        if (index < 0 || index > size) {
            throw new ArrayIndexOutOfBoundsException(index);
        }

        ensureCapacity();

        // 进行搬移
        // i 代表 原位置
        // 搬的动作：array[i + 1] = array[i]
//        for (int i = size - 1; i >= index; i--) {
//            array[i + 1] = array[i];
//        }

        // i 代表 目标位置
        // 搬：array[i] = array[i - 1]
        for (int i = size; i > index; i--) {
            array[i] = array[i - 1];
        }

        array[index] = e;

        size = size + 1;
    }

    // O(n)
    public Long remove(int index) {
        if (index < 0 || index >= size) {
            throw new ArrayIndexOutOfBoundsException(index);
        }

        Long e = array[index];

        // i 代表原位置
        // array[i - 1] = array[i]
        // [index + 1, size - 1]
//        for (int i = index + 1; i <= size - 1; i++) {
//            array[i - 1] = array[i];
//        }

        // i 代表目标位置
        // array[i] = array[i + 1]
        // [index, size - 2]
        for (int i = index; i <= size - 2; i++) {
            array[i] = array[i + 1];
        }
        // System.arraycopy   // 不会产生新的数组对象
        // Arrays.copyOfRange  // 复制并产生一个新的对象

        array[size - 1] = null;

        size = size - 1;

        return e;
    }

    // O(n)
    public boolean remove(Long e) {
        // 找到从前往后的第一个 e（第一个 和 e 相等的 元素）并删除

        for (int i = 0; i < size; i++) {
            // 顺序表当前要比较的元素 array[i]，目标元素：e
            // array[i]   Long       e  Long
            // 比较两个元素的相等性   equals 去比较
            // 引用 == 比较：同一性

            // 解引用操作（dereference）：根据引用找到对象，然后操作对象
            // 解引用合法的前提：引用得指向某个对象  r != null
            // 解引用的时候，出现 r == null，规定 NullPointerException
            // x[n]
            // r.m
            // r.m()

            // array[]  array 肯定不为 null
            // array[i] 语法上没有保证过，但语义上咱认为不会是 null
            if (array[i].equals(e)) {
                // 删除 array[i]
                remove(i);  // O(n)

                return true;
            }
        }

        return false;
    }

    // O(1)
    public Long get(int index) {
        if (index < 0 || index >= size) {
            throw new ArrayIndexOutOfBoundsException(index);
        }

        return array[index];
    }

    // O(1)
    public Long set(int index, Long e) {
        if (index < 0 || index >= size) {
            throw new ArrayIndexOutOfBoundsException(index);
        }

        Long o = array[index];

        array[index] = e;

        return o;
    }

    public void clear() {
        for (int i = 0; i < size; i++) {
            array[i] = null;
        }
        size = 0;
    }

    public int indexOf(Long e) {
        for (int i = 0; i < size; i++) {
            if (array[i].equals(e)) {
                return i;
            }
        }

        return -1;
    }

    public int lastIndexOf(Long e) {
        for (int i = size - 1; i >= 0; i--) {
            if (array[i].equals(e)) {
                return i;
            }
        }

        return -1;
    }

    public boolean contains(Long e) {
        return indexOf(e) != -1;
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    // 测试
    // 1. 手工测
    // 2. 自动测试（单元测试）

    public void 是否是一个合法的顺序表对象() {
        if (size < 0) {
            throw new RuntimeException("size 小于 0 了");
        }

        if (array == null) {
            throw new RuntimeException("array 是 null 了");
        }

        if (size > array.length) {
            throw new RuntimeException("size > capacity 了");
        }

        for (int i = 0; i < size; i++) {
            if (array[i] == null) {
                throw new RuntimeException("array[0, size) 出现了 null 了, i =" + i);
            }
        }

        for (int i = size; i < array.length; i++) {
            if (array[i] != null) {
                throw new RuntimeException("array[size, capacity) 出现了 !null 了, i =" + i);
            }
        }
    }

    public void 检查元素情况(long[] expectElements) {
        if (size != expectElements.length) {
            throw new RuntimeException("当前 size 不符合 预期 size。size = " + size);
        }

        for (int i = 0; i < size; i++) {
            // 等号左边是 Long，等号右边是 long
            // 所以编译器会自动拆箱
            // 就不用调用 equals 了
            if (array[i] != expectElements[i]) {
                throw new RuntimeException("元素和预期的元素不符，i = " + i);
            }
        }
    }

    public static void main2(String[] args) {
        MyArrayList list = new MyArrayList();
        list.add(1L);
        list.add(1L);
        list.add(1L);
        list.add(1L);
        list.add(1L);
        list.add(1L);
        list.add(1L);
        list.add(1L);
        list.add(1L);
    }

    public static void main1(String[] args) {
        MyArrayList list = new MyArrayList();
        list.是否是一个合法的顺序表对象();
        list.检查元素情况(new long[0]);   // 期待一开始元素是 0 个
        System.out.println(list.isEmpty()); // true

        list.add(100L);
        list.是否是一个合法的顺序表对象();
        list.检查元素情况(new long[] { 100 });   // 期待元素 { 100 }
        System.out.println(list.isEmpty()); // false

        list.add(200L);
        list.是否是一个合法的顺序表对象();
        list.检查元素情况(new long[] { 100, 200 });   // 期待元素 { 100, 200 }

        list.add(300L);
        list.是否是一个合法的顺序表对象();
        list.检查元素情况(new long[] { 100, 200, 300 });   // 期待元素 { 100, 200, 300 }

        list.add(0, 500L);
        list.是否是一个合法的顺序表对象();
        list.检查元素情况(new long[] { 500, 100, 200, 300 });   // 期待元素 { 500, 100, 200, 300 }

        list.add(list.size(), 600L);
        list.是否是一个合法的顺序表对象();
        list.检查元素情况(new long[] { 500, 100, 200, 300, 600 });

        list.add(3, 700L);
        list.是否是一个合法的顺序表对象();
        list.检查元素情况(new long[] { 500, 100, 200, 700, 300, 600 });

        long e = list.remove(0);
        list.是否是一个合法的顺序表对象();
        list.检查元素情况(new long[] { 100, 200, 700, 300, 600 });
        System.out.println(e);      // 500

        e = list.remove(list.size() - 1);
        list.是否是一个合法的顺序表对象();
        list.检查元素情况(new long[] { 100, 200, 700, 300 });
        System.out.println(e);      // 600

        e = list.remove(2);
        list.是否是一个合法的顺序表对象();
        list.检查元素情况(new long[] { 100, 200, 300 });
        System.out.println(e);      // 700

        list.remove(0);
        list.remove(0);
        e = list.remove(0);
        list.是否是一个合法的顺序表对象();
        list.检查元素情况(new long[] { });
        System.out.println(e);      // 300



        list.clear();
        list.是否是一个合法的顺序表对象();
        list.检查元素情况(new long[0]);   // 期待一开始元素是 0 个
        System.out.println(list.isEmpty()); // true


        list.add(1000L);
        list.add(2000L);
        list.add(1000L);
        list.add(2000L);
        list.add(1000L);
        list.add(2000L);
        // [1000, 2000, 1000, 2000, 1000, 2000]

        System.out.println(list.indexOf(2000L));    // [1]
        System.out.println(list.lastIndexOf(2000L));    // [5]

        System.out.println(list.remove(2000L)); // true
        list.是否是一个合法的顺序表对象();
        list.检查元素情况(new long[] { 1000L, 1000L, 2000L, 1000L, 2000L });
    }
}
