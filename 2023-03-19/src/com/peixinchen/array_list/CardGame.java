package com.peixinchen.array_list;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CardGame {
    public static void main(String[] args) {
        ArrayList<Card> cardList = new ArrayList<>(52);

        String[] suits = {"♥", "♠", "♦", "♣"};
        for (String suit : suits) {
            for (int rank = 1; rank <= 13; rank++) {
                Card card = new Card(suit, rank);

                cardList.add(card);
            }
        }

        // 洗牌
        Collections.shuffle(cardList);

//        System.out.println(cardList);

        // 假设有 5 名玩家，依次抓牌，每人一共抓 3 张
        List<Card> player1 = new ArrayList<>();
        List<Card> player2 = new ArrayList<>();
        List<Card> player3 = new ArrayList<>();
        List<Card> player4 = new ArrayList<>();
        List<Card> player5 = new ArrayList<>();

        for (int i = 0; i < 3; i++) {
            Card card;

            // 抓拍
            card = cardList.remove(cardList.size() - 1);
            // 放入手中

            player1.add(card);

            card = cardList.remove(cardList.size() - 1);
            player2.add(card);

            card = cardList.remove(cardList.size() - 1);
            player3.add(card);

            card = cardList.remove(cardList.size() - 1);
            player4.add(card);

            card = cardList.remove(cardList.size() - 1);
            player5.add(card);
        }

        System.out.println(player1.get(1));
        System.out.println(player2.get(1));
        System.out.println(player3.get(1));
        System.out.println(player4.get(1));
        System.out.println(player5.get(1));

//        System.out.println(player1);
//        System.out.println(player2);
//        System.out.println(player3);
//        System.out.println(player4);
//        System.out.println(player5);

        cardList.addAll(player1);
        player1.clear();

        // ...
    }
}
