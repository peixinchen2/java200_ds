package com.peixinchen.stack;

public class Node {
    int val;
    Node next;

    Node(int val) {
        this.val = val;
    }
}
