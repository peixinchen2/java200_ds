package com.peixinchen.stack;

public class 逆序打印单链表 {
    public static void main(String[] args) {
        Node head = 构建链表();

        递归逆序打印(head);
    }

    private static void 递归逆序打印(Node head) {
        if (head == null) {
            System.out.println("null");
            return;
        }

        // 不是空的链表
        // 把一个问题 -> 一个子问题
        递归逆序打印(head.next);
        System.out.println(head.val);
    }

    private static Node 构建链表() {
        Node n1 = new Node(1);
        Node n2 = new Node(2);
        Node n3 = new Node(3);
        Node n4 = new Node(4);
        Node n5 = new Node(5);

        n1.next = n2;
        n2.next = n3;
        n3.next = n4;
        n4.next = n5;

        return n1;
    }
}
