package com.peixinchen;

import java.util.Random;

public class BubbleSort {
    // 时间复杂度: O(n^2)
    public static void sort(long[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = 0; j < array.length - i - 1; j++) {
                if (array[i] > array[i + 1]) {
                    long t = array[i];
                    array[i] = array[j];
                    array[j] = t;
                }
            }
        }
    }

    // 泛型方法的定义
    // 返回值类型前面的尖括号 <E>：定义一个类 类型变量，名称叫做 E（名字叫什么无所谓）
    public static <E> void sort(E[] array) {
        // 不具体实现了
    }

    // 泛型方法的使用
    public static void test() {
        // 标准写法
        Random[] array = new Random[10];

        BubbleSort.<Random>sort(array); // 指定了变化的类型是 Random

        BubbleSort.sort(array); // 省略版本
    }

//    public static void sort(Object[] array) {
//        for (int i = 0; i < array.length - 1; i++) {
//            for (int j = 0; j < array.length - i - 1; j++) {
//                if (array[i] > array[i + 1]) {  // 引用类型的比较不直接使用 > 比较，但是有统一方法
//                    Object t = array[i];
//                    array[i] = array[j];
//                    array[j] = t;
//                }
//            }
//        }
//    }

    public static void main(String[] args) {
        long[] array = new long[10_0000];
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextLong();
        }
        // 时间戳（毫秒）：从某个时刻（1970-01-01 日）开始到现在经过的毫秒数
        long s = System.currentTimeMillis();    // 获取当前的时间戳（timestamp）以 毫秒 为单位
        sort(array);
        long e = System.currentTimeMillis();
        long ms = e - s;    // 排序耗时，单位是毫秒
        double sec = ms / 1000.0;
        System.out.printf("排序经过 %.2f 秒\n", sec);
    }
}
