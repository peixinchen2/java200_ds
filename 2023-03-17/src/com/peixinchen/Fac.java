package com.peixinchen;

public class Fac {
    public static long fac(int n) {
        if (n == 1) {
            return n;
        }

        long r = fac(n - 1);
        return r * n;
    }

    public static void main(String[] args) {
        fac(5);
    }
}
