package com.peixinchen;

// 定义泛型方法（接口同理）
public class GenericClass<T1, T2, T3> {
    T1 a;
    T2 b;
    T3 c;
}
