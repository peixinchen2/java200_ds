package com.peixinchen;

import java.util.Random;
import java.util.Scanner;

// 要求大家掌握
public class UseGenericClass {
    public static void main(String[] args) {
        // Random, String, Scanner

        // 完整写法
        // 引用上通过尖括号体现类型
        // new 对象时，类名后边体现类型
        GenericClass<Random, String, Scanner> r;
        r = new GenericClass<Random, String, Scanner>();

        GenericClass<Random, String, Scanner> s = new GenericClass<Random, String, Scanner>();

        // 常见写法
        // new 的时候类型可以省略，尖括号不能省略
        GenericClass<Random, String, Scanner> t = new GenericClass<>();
    }
}
