package com.peixinchen.linked_list;

public class ListNode {
    Long val;
    ListNode prev;
    ListNode next;

    ListNode(Long val) {
        this.val = val;
    }
}
