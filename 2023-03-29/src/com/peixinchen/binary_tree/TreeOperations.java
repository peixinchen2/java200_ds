package com.peixinchen.binary_tree;

import java.util.LinkedList;
import java.util.Queue;

public class TreeOperations {
    public static boolean contains(TreeNode root, int target) {
        // 使用前序的方式进行搜索
        // 1. 先判断根的元素是否是 target，一旦找到，返回找到
        // 2. 如果根不是，去左子树查找，一旦找到，返回找到
        // 3. 最后去右子树查找
        if (root == null) {
            return false;
        }

        if (root.val == target) {
            return true;
        }

        boolean leftContains = contains(root.left, target);    // 左子树查找
        if (leftContains) {
            return true;
        }

        boolean rightContains = contains(root.right, target);
        return rightContains;
    }

    public static TreeNode nodeOf(TreeNode root, int target) {
        if (root == null) {
            return null;
        }

        if (root.val == target) {
            return root;
        }

        TreeNode leftResult = nodeOf(root.left, target);
        if (leftResult != null) {
            // 左子树中找到了
            // 应该返回什么？
            return leftResult;
        }

        return nodeOf(root.right, target);
    }

    public static boolean containsNode(TreeNode root, TreeNode target) {
        if (root == null) {
            return false;
        }

        // root 和 target 指向同一个对象
        // root 和 target 是同一个结点
        if (root == target) {
            return true;
        }

        if (containsNode(root.left, target)) {
            return true;
        }

        return containsNode(root.right, target);
    }

    // null 写成 #
    public static void levelOrderWithNull(TreeNode root) {
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);  // 哪怕 root == null，也放入队列

        while (!queue.isEmpty()) {
            TreeNode node = queue.poll();
            if (node == null) {
                System.out.print("# ");
            } else {
                System.out.print(node.val + " ");
                queue.offer(node.left);
                queue.offer(node.right);
            }
        }

        System.out.println();
    }

    public static void levelOrder(TreeNode root) {
        if (root == null) {
            return;
        }

        Queue<TreeNode> queue = new LinkedList<>();
        // 一开始先把根放入结点，开启过程
        queue.offer(root);

        while (!queue.isEmpty()) {
            TreeNode node = queue.poll();
            System.out.print(node.val + " ");
            if (node.left != null) {
                queue.offer(node.left);
            }

            if (node.right != null) {
                queue.offer(node.right);
            }
        }

        System.out.println();
    }

    public static void main(String[] args) {
        {
            System.out.println("不是完全二叉树");
            TreeNode root = buildTree1();
            levelOrderWithNull(root);
            System.out.println(isCompleteTree(root));
        }

        {
            System.out.println("完全二叉树");
            TreeNode root = buildTree2();
            levelOrderWithNull(root);
            System.out.println(isCompleteTree(root));
        }
    }

    public static boolean isCompleteTree(TreeNode root) {
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);

        // 取出 null 即可以停止了
        while (true) {
            TreeNode node = queue.poll();
            if (node == null) {
                break;
            }

            queue.offer(node.left);
            queue.offer(node.right);
        }

        while (!queue.isEmpty()) {
            TreeNode node = queue.poll();
            if (node != null) {
                return false;
            }
        }

        return true;
    }

    private static TreeNode buildTree1() {
        TreeNode n1 = new TreeNode(1);
        TreeNode n2 = new TreeNode(2);
        TreeNode n3 = new TreeNode(3);
        TreeNode n4 = new TreeNode(4);
        TreeNode n5 = new TreeNode(5);
        TreeNode n6 = new TreeNode(6);
        TreeNode n7 = new TreeNode(7);
        TreeNode n8 = new TreeNode(8);

        n1.left = n2;   n1.right = n3;
        n2.left = n4;   n2.right = n5;
        n3.right = n6;
        n6.left = n7;   n6.right = n8;

        return n1;
    }

    private static TreeNode buildTree2() {
        TreeNode n1 = new TreeNode(1);
        TreeNode n2 = new TreeNode(2);
        TreeNode n3 = new TreeNode(3);
        TreeNode n4 = new TreeNode(4);
        TreeNode n5 = new TreeNode(5);
        TreeNode n6 = new TreeNode(6);
        TreeNode n7 = new TreeNode(7);
        TreeNode n8 = new TreeNode(8);

        n1.left = n2;   n1.right = n3;
        n2.left = n4;   n2.right = n5;
        n3.left = n6;   n3.right = n7;
        n4.left = n8;

        return n1;
    }
}
