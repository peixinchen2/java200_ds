package com.peixinchen;

import java.util.Arrays;

public class Partition {
    private static void swap(long[] array, int i, int j) {
        long t = array[i];
        array[i] = array[j];
        array[j] = t;
    }

    public static void partition1(long[] array) {
        // [奇数][未判断][偶数]
        int i = 0;
        int j = array.length;
        // 初始情况下，奇数区间内一个元素都没有，偶数区间内一个元素都没有
        // 奇数: [0, i)              [0, 0)
        // 未判断: [i, j)            [0, array.length)
        // 偶数: [j, array.length)   [array.lenght, array.length)

        while (i < j) {
            while (i < j && array[i] % 2 != 0) {
                i++;
            }

            while (i < j && array[j - 1] % 2 == 0) {
                j--;
            }

            if (i < j) {
                swap(array, i, j - 1);
            }
        }
    }

    public static void partition(long[] array) {
        // [奇数][偶数][未判断]
        int i = 0;
        int j = 0;
        // 初始情况下，奇数区间内一个元素都没有，偶数区间内一个元素都没有
        // 奇数: [0, i)                [0, 0)
        // 偶数: [i, j)                [0, 0)
        // 未判断: [j, array.length)   [0, array.length)

        while (j < array.length) {
            if (array[j] % 2 == 0) {
                j++;
            } else {
                swap(array, i, j);
                i++;
                j++;
            }
        }
    }

    public static void main(String[] args) {
        long[] array;

        array = new long[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        partition(array);
        System.out.println(Arrays.toString(array));

        array = new long[] { 1, 3, 5, 7, 9, 2, 4, 6, 8, 10 };
        partition(array);
        System.out.println(Arrays.toString(array));

        array = new long[] { 2, 4, 6, 8, 10, 1, 3, 5, 7, 9 };
        partition(array);
        System.out.println(Arrays.toString(array));

        array = new long[] { 1, 3, 5, 7, 9 };
        partition(array);
        System.out.println(Arrays.toString(array));

        array = new long[] { 2, 4, 6, 8, 10 };
        partition(array);
        System.out.println(Arrays.toString(array));
    }
}
