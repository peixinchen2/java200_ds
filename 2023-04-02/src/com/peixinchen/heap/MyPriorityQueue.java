package com.peixinchen.heap;

import java.util.Arrays;

// 动态地、找最值
public class MyPriorityQueue {
    private long[] array;
    private int size;

    public MyPriorityQueue() {
        // 暂时不考虑扩容了
        array = new long[100];
        size = 0;
    }

    public MyPriorityQueue(long[] array, int size) {
        this.array = Arrays.copyOf(array, 100);
        this.size = size;
        HeapOperations.createHeap(this.array, this.size);
    }

    // O(log(n))
    public Long poll() {
        if (size <= 0) {
            throw new RuntimeException("优先队列是空的");
        }

        long e = array[0];
        array[0] = array[size - 1];
        this.size = this.size - 1;
        HeapOperations.shiftDown(this.array, this.size, 0);
        return e;
    }

    public Long peek() {
        if (size <= 0) {
            throw new RuntimeException("优先队列是空的");
        }

        return array[0];
    }

    public void offer(long e) {
        array[size++] = e;
        HeapOperations.shiftUp(array, size - 1);
    }
}
