package com.peixinchen.heap;

import java.util.Arrays;

public class HeapOperations {
    // O(log(n))
    public static void shiftUp(long[] array, int index) {
        while (true) {
            if (index == 0) {
                return;
            }

            int parentIdx = (index - 1) / 2;
            if (array[parentIdx] >= array[index]) {
                return;
            }

            long t = array[index];
            array[index] = array[parentIdx];
            array[parentIdx] = t;

            index = parentIdx;
        }
    }

    // O(log(n))
    // 需要的参数：
    // 1. 堆（数组）     long[] array, int size
    // 2. 要调整的位置   int index
    public static void shiftDown(long[] array, int size, int index) {
        while (true) {
            // 1. 判断这个位置是否已经满足堆的性质
            // 1.1 只要该位置已经是叶子了，就不需要做处理
            //     逻辑：叶子 <-> 一个孩子都没有 <-> 没有左孩子
            //     物理上是数组：左孩子的下标越界 <-> 没有左孩子
            int leftIdx = index * 2 + 1;
            if (leftIdx >= size) {
                return;
            }

            int maxIdx = leftIdx;   // 假设最大的孩子是左孩子，什么情况下假设不成立？
            // 存在右孩子 && 右孩子 > 左孩子
            if (leftIdx + 1 < size && array[leftIdx + 1] > array[leftIdx]) {
                maxIdx = leftIdx + 1;
            }

            if (array[index] >= array[maxIdx]) {
                // 满足堆的性质
                return;
            }

            long t = array[index];
            array[index] = array[maxIdx];
            array[maxIdx] = t;

            // 交换之后不算完
            index = maxIdx;
        }
    }

    // O(n * log(n))
    // O(n)
    public static void createHeap(long[] array, int size) {
        for (int i = (size - 2) / 2; i >= 0; i--) {
            shiftDown(array, size, i);
        }
    }

    public static void main(String[] args) {
//        long[] array = { 5, 3, 5, 5, 5, 5, 5, 5, 5, 0, 0, 0, 0 };
//        int index = 1;
//        shiftDown(array, 9, index);
//        System.out.println(Arrays.toString(array));

//        long[] array = {3, 9, 7, 6, 8, 8, 2, 4, 1, 7, 100, 100, 100};
//        int size = 10;
//
//        createHeap(array, size);
//
//        System.out.println(Arrays.toString(array));
    }
}
