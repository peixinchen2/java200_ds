// ACM 模式的 OJ 题
// 1）主类的名字必须是 Main
// 2) import java.util.*
// 3）理解输入的格式

import java.util.*;

class TreeNode {
    public char val;
    public TreeNode left;
    public TreeNode right;

    public TreeNode(char val) {
        this.val = val;
    }
}

public class Main {
    private static List<Character> toList(String s) {
        List<Character> list = new ArrayList<>();
        for (char ch : s.toCharArray()) {
            list.add(ch);
        }
        return list;
    }

    private static TreeNode buildTree(List<Character> preorder) {
        // 特殊情况
        if (preorder.isEmpty()) {
            return null;
        }

        char rootVal = preorder.remove(0);  // remove vs get
        if (rootVal == '#') {
            return null;
        }

        // 不是空树
        TreeNode root = new TreeNode(rootVal);

        // 构建左子树
        root.left = buildTree(preorder);    // 传的虽然是 preorder，但序列已经不同了
        // 构建右子树
        root.right = buildTree(preorder);   // 传的虽然是 preorder，但序列已经不同了

        return root;
    }

    private static void inorderTraversal(TreeNode root) {
        if (root == null) {
            return;
        }

        inorderTraversal(root.left);
        System.out.print(root.val + " ");
        inorderTraversal(root.right);
    }

    public static void main(String[] args) {
        // 如果有多行输入
        Scanner scanner = new Scanner(System.in);
        // scanner.hasNextLine()   boolean
        // scanner.nextLine()      String
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            // 把 String 转成 List<Character>
            List<Character> preorder = toList(line);
            TreeNode root = buildTree(preorder);
            inorderTraversal(root);
            System.out.println();
        }
    }
}

