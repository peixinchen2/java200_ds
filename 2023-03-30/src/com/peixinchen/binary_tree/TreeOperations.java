package com.peixinchen.binary_tree;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class TreeOperations {
    // 前序遍历（非递归版本）
    public static void preorder(TreeNode root) {
        TreeNode cur = root;
        Deque<TreeNode> stack = new LinkedList<>();

        while (cur != null || !stack.isEmpty()) {
            while (cur != null) {
                System.out.println(cur.val);
                stack.push(cur);
                cur = cur.left;
            }

            TreeNode top = stack.pop();
            if (top.right != null) {
                cur = top.right;
            } else {
                cur = null;
            }
        }
    }

    // 中序遍历（非递归版本）
    public static void inorder(TreeNode root) {
        TreeNode cur = root;
        Deque<TreeNode> stack = new LinkedList<>();

        while (cur != null || !stack.isEmpty()) {
            while (cur != null) {
                stack.push(cur);
                cur = cur.left;
            }

            TreeNode top = stack.pop();
            System.out.println(top.val);
            if (top.right != null) {
                cur = top.right;
            } else {
                cur = null;
            }
        }
    }

    // 后序遍历（非递归版本）
    public static void postorder(TreeNode root) {
        TreeNode cur = root;
        Deque<TreeNode> stack = new LinkedList<>();
        TreeNode last = null;   // 上一次后序遍历的结点

        while (cur != null || !stack.isEmpty()) {
            while (cur != null) {
                stack.push(cur);
                cur = cur.left;
            }

//            TreeNode top = stack.pop();   // 从左边回来时不能直接出栈
            TreeNode top = stack.peek();
            if (top.right == null) {
                // 右孩子为 null，视为右孩子已经处理了
                // 所以 top 的左右都被处理
                // 所以 top 可以出栈了
                stack.pop();
                System.out.println(top.val);

                last = top;
            } else if (top.right == last) {
                // 说明上一次遍历的结点就是 top 的 right
                // 即 右孩子已经处理了
                // 所以 top 的左右都被处理
                // 所以 top 可以出栈了
                stack.pop();
                System.out.println(top.val);

                last = top;
            } else {
                // 说明是从 top 左边回来的
                cur = top.right;
            }
        }
    }

    public static void printPath(TreeNode root) {
        TreeNode cur = root;
        Deque<TreeNode> stack = new LinkedList<>();
        TreeNode last = null;   // 上一次后序遍历的结点

        while (cur != null || !stack.isEmpty()) {
            while (cur != null) {
                stack.push(cur);
                cur = cur.left;
            }

//            TreeNode top = stack.pop();   // 从左边回来时不能直接出栈
            TreeNode top = stack.peek();
            if (top.right == null) {
                // 右孩子为 null，视为右孩子已经处理了
                // 所以 top 的左右都被处理
                // 所以 top 可以出栈了
                List<TreeNode> path = new ArrayList<>(stack);
                System.out.println(path);
                stack.pop();

                last = top;
            } else if (top.right == last) {
                // 说明上一次遍历的结点就是 top 的 right
                // 即 右孩子已经处理了
                // 所以 top 的左右都被处理
                // 所以 top 可以出栈了
                List<TreeNode> path = new ArrayList<>(stack);
                System.out.println(path);
                stack.pop();

                last = top;
            } else {
                // 说明是从 top 左边回来的
                cur = top.right;
            }
        }
    }

    public static void levelOrderWithLevel(TreeNode root) {
        if (root == null) {
            return;
        }
        // 结点 <-> level
        // 放元素的时候，同时向两个队列中添加元素即可
        Queue<TreeNode> nodeQueue = new LinkedList<>();
        Queue<Integer> levelQueue = new LinkedList<>();

        nodeQueue.offer(root);
        levelQueue.offer(0);

        while (!nodeQueue.isEmpty()) {
            TreeNode node = nodeQueue.poll();
            int level = levelQueue.poll();

            System.out.println(level + ": " + node);

            if (node.left != null) {
                nodeQueue.offer(node.left);
                levelQueue.offer(level + 1);
            }

            if (node.right != null) {
                nodeQueue.offer(node.right);
                levelQueue.offer(level + 1);
            }
        }
    }

    public static void main(String[] args) {
        TreeNode root = buildTree();
        levelOrderWithLevel(root);
    }

    private static TreeNode buildTree() {
        TreeNode n3 = new TreeNode(3);
        TreeNode n9 = new TreeNode(9);
        TreeNode n20 = new TreeNode(20);
        TreeNode n15 = new TreeNode(15);
        TreeNode n7 = new TreeNode(7);

        n3.left = n9;   n3.right = n20;
        n20.left = n15;
        n20.right = n7;

        return n3;
    }
}
