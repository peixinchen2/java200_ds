package com.peixinchen.binary_tree;

// 此类是二叉树的结点类，不是二叉树类
public class TreeNode {
    public int val;
    public TreeNode left;   // 该结点的左孩子   null <=> 左孩子不存在
    public TreeNode right;  // 该结点的右孩子   null <=> 右孩子不存在

    public TreeNode(int val) {
        this.val = val;
    }
}
